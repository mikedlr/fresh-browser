#!/bin/sh
#Copyright 2016 Michael De La Rue
#See file README for license terms (AGPLv3)
TMPDIR=`mktemp --directory --tmpdir ffoxdir.XXXXXXXXXX`
firefox  -no-remote  -profile $TMPDIR

#we assume that the tmpdir is autocleaned so we leave the directory so
#it's useful in later debugging.  Don't run this in a directory which
#is not autocleaned ;-)
